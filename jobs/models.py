from django.db import models
from datetime import datetime

class Job(models.Model):
    image = models.ImageField(upload_to='images/')
    summary = models.CharField(max_length=200)
    hiredate= models.DateTimeField(default=datetime.now, blank=True)
    todate = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.summary

    def hiredate_pretty(self):
        return self.hiredate.strftime('%B %-d, %Y')

    def todate_pretty(self):
        return self.todate.strftime('%B %-d, %Y')
